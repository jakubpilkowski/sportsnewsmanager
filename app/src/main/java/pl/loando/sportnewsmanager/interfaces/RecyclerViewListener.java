package pl.loando.sportnewsmanager.interfaces;

public interface RecyclerViewListener {
    void onRemoveItem(int position);
}
