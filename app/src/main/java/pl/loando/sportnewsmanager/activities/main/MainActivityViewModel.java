package pl.loando.sportnewsmanager.activities.main;

import android.content.Intent;
import android.util.Log;
import android.view.View;

import androidx.databinding.ObservableBoolean;

import pl.loando.sportnewsmanager.activities.addnews.AddNewsActivity;
import pl.loando.sportnewsmanager.base.BaseViewModel;

public class MainActivityViewModel extends BaseViewModel {
    public static final String TAG = "MainActivityViewModel";
    public ObservableBoolean showFab = new ObservableBoolean();
    public void init() {
        showFab.set(true);
    }
    public void onFabClick(){
            Intent intent = new Intent(getActivity().getApplicationContext(), AddNewsActivity.class);
            getActivity().startActivityForResult(intent, MainActivity.REQUEST_CODE);
    }
}
