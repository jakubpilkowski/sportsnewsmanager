package pl.loando.sportnewsmanager.activities.main;

import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.activities.settings.SettingsActivity;
import pl.loando.sportnewsmanager.base.BaseActivity;
import pl.loando.sportnewsmanager.base.BaseFragment;
import pl.loando.sportnewsmanager.databinding.ActivityMainBinding;
import pl.loando.sportnewsmanager.interfaces.Providers;
import pl.loando.sportnewsmanager.models.UserPreferences;
import pl.loando.sportnewsmanager.navigation.Navigator;
import pl.loando.sportnewsmanager.ui.home.HomeFragment;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainActivityViewModel> implements Providers {
    public static final int REQUEST_CODE = 1001;

    @Override
    protected void initActivity(final ActivityMainBinding binding) {
        binding.setViewModel(viewModel);
        viewModel.setProviders(this);
        navigator.showHome();
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        UserPreferences.init(UserPreferences.SETTINGS, getApplicationContext());
        viewModel.init();
        binding.bottomNavigationView.setItemIconTintList(null);
    }

    @Override
    public Class<MainActivityViewModel> getViewModel() {
        return MainActivityViewModel.class;
    }

    @Override
    public int getIdFragmentContainer() {
        return R.id.container;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    public Navigator getNavigator() {
        return navigator;
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public FragmentManager getManager() {
        return getSupportFragmentManager();
    }

    @Override
    public ViewDataBinding getBinding() {
        return binding;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        binding.bottomNavigationView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                ((HomeFragment) getCurrentFragment()).addTeam();
            }
        }
    }

    @Override
    public ViewDataBinding getFragmentBinding() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(getIdFragmentContainer());
        return ((BaseFragment) fragment).binding;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.toolbar_menu_settings:
                View view = binding.toolbar.findViewById(R.id.toolbar_menu_settings);
                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_0_to_120);
                view.startAnimation(animation);
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (getCurrentFragment() instanceof BaseFragment)
            switch (((BaseFragment) getCurrentFragment()).getHomeTypeButton()) {
                case 0:
                    onBackPressed();
                    refreshHomeButton();
                    break;
                case 1:
                    return false;

            }
        return true;
    }

}
