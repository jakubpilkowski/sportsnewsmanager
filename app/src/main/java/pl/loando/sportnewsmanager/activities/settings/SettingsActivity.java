package pl.loando.sportnewsmanager.activities.settings;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;

import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentManager;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.base.BaseActivity;
import pl.loando.sportnewsmanager.base.BaseFragment;
import pl.loando.sportnewsmanager.databinding.ActivitySettingsBinding;
import pl.loando.sportnewsmanager.databinding.SettingsFragmentBinding;
import pl.loando.sportnewsmanager.interfaces.Providers;
import pl.loando.sportnewsmanager.models.UserPreferences;
import pl.loando.sportnewsmanager.navigation.Navigator;

public class SettingsActivity extends BaseActivity<ActivitySettingsBinding, SettingsActivityViewModel> implements Providers {

    @Override
    protected void initActivity(ActivitySettingsBinding binding) {
        binding.setViewModel(viewModel);
        setSupportActionBar(binding.settingsToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        UserPreferences.init(UserPreferences.SETTINGS, getApplicationContext());
        navigator.showSettingsFragment();
    }

    @Override
    public Class<SettingsActivityViewModel> getViewModel() {
        return SettingsActivityViewModel.class;
    }

    @Override
    public int getIdFragmentContainer() {
        return R.id.settings_container;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_settings;
    }
    @Override
    public boolean onSupportNavigateUp() {
        if(getCurrentFragment() instanceof BaseFragment)
            switch (((BaseFragment) getCurrentFragment()).getHomeTypeButton()){
                case 0:
                    onBackPressed();
                    refreshHomeButton();
                    break;
                case 1:
                    return false;
            }
        return true;
    }

    @Override
    public Navigator getNavigator() {
        return navigator;
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public FragmentManager getManager() {
        return getSupportFragmentManager();
    }

    @Override
    public ViewDataBinding getBinding() {
        return binding;
    }

    @Override
    public ViewDataBinding getFragmentBinding() {
        return null;
    }
}
