package pl.loando.sportnewsmanager.navigation;

import android.content.Intent;
import android.net.Uri;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.ui.newschooser.NewsChooserFragment;
import pl.loando.sportnewsmanager.ui.newsmanagement.newsmanagement.NewsManagementFragment;
import pl.loando.sportnewsmanager.ui.home.HomeFragment;
import pl.loando.sportnewsmanager.ui.popularsites.PopularSitesFragment;
import pl.loando.sportnewsmanager.ui.settings.SettingsFragment;
import pl.loando.sportnewsmanager.ui.spinnerview.SpinnerViewFragment;
import pl.loando.sportnewsmanager.ui.yournews.YourNewsFragment;

public class Navigator {
    private static final String INITIAL_BACKSTACK = "INIT";
    private FragmentActivity activity;
    private YourNewsFragment yourNewsFragment = YourNewsFragment.newInstance();
    private NewsManagementFragment addNewsFragment = NewsManagementFragment.newInstance();
    private PopularSitesFragment popularSitesFragment = PopularSitesFragment.newInstance();

    public void setActivity(FragmentActivity activity) {
        this.activity = activity;
    }

    public void init() {
        activity.getSupportFragmentManager().beginTransaction().addToBackStack(INITIAL_BACKSTACK).commit();
    }

    public void showHome() {
        activity.getSupportFragmentManager().beginTransaction()
                .addToBackStack(HomeFragment.TAG)
                .replace(R.id.container, HomeFragment.newInstance(), HomeFragment.TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    public void showSettingsFragment() {
        activity.getSupportFragmentManager().beginTransaction().addToBackStack(SettingsFragment.TAG)
                .replace(R.id.settings_container, SettingsFragment.newInstance(), SettingsFragment.TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    public void openSite(String uri) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(uri));
        activity.startActivity(intent);

    }

    public void showNewsChooserContent(int navigateTo) {
        activity.getSupportFragmentManager().beginTransaction()
                .add(R.id.add_news_container, NewsChooserFragment.newInstance(navigateTo), NewsChooserFragment.TAG)
                .addToBackStack(NewsChooserFragment.TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    public void openNewsChooser() {
        activity.getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right)
                .add(R.id.add_news_container, NewsChooserFragment.newInstance(1), NewsChooserFragment.TAG)
                .addToBackStack(NewsChooserFragment.TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    public void openSpinnerItemView(String tag) {
        activity.getSupportFragmentManager()
                .beginTransaction().replace(R.id.settings_container, SpinnerViewFragment.newInstance(tag), SpinnerViewFragment.TAG)
                .addToBackStack(SpinnerViewFragment.TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }
}
