package pl.loando.sportnewsmanager.adapters;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.badge.BadgeDrawable;

import java.util.ArrayList;
import java.util.List;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.databinding.ActivityMainBinding;
import pl.loando.sportnewsmanager.databinding.SingleNewsBinding;
import pl.loando.sportnewsmanager.interfaces.BadgeListener;
import pl.loando.sportnewsmanager.models.News;
import pl.loando.sportnewsmanager.navigation.Navigator;

public class MyNewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements BadgeListener {
    private static final int TYPE_INITIAL = 0;
    static final int TYPE_NEWS = 1;
    private List<News> news = new ArrayList<>();
    private Navigator navigator;
    private List<MyNewsAdapterViewModel> viewModels = new ArrayList<>();
    private ViewDataBinding binding;
    private int tmpBadgeValue;

    public void setNews(List<News> news) {
        this.news.clear();
        this.news.addAll(news);
        tmpBadgeValue = news.size();
        notifyDataSetChanged();
        setBadgeValue(TYPE_INITIAL);
    }

    public void setNavigator(Navigator navigator) {
        this.navigator = navigator;
    }

    public void setActivityBinding(ViewDataBinding binding) {
        this.binding = binding;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_news, parent, false);
        SingleNewsBinding binding = SingleNewsBinding.bind(itemView);
        return new SingleNewsViewHolder(itemView, binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyNewsAdapterViewModel viewModel;

        if (viewModels.size() <= position) {
            viewModel = new MyNewsAdapterViewModel();
            viewModels.add(viewModel);
            ((SingleNewsViewHolder) holder).getBinding().setViewModel(viewModel);
            viewModel.setListener(this);
            viewModel.init(news.get(position), navigator);

        } else {
            viewModel = viewModels.get(position);
            ((SingleNewsViewHolder) holder).getBinding().setViewModel(viewModel);
        }
        if (position + 1 == getItemCount()) {
            setBottomMargin(holder.itemView, (int) (86 * Resources.getSystem().getDisplayMetrics().density));
        } else {
            setBottomMargin(holder.itemView, 0);
        }
    }

    private static void setBottomMargin(View view, int bottomMargin) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            params.setMargins(params.leftMargin, params.topMargin, params.rightMargin, bottomMargin);
            view.requestLayout();
        }
    }

    @Override
    public int getItemCount() {
        return news.size();
    }

    @Override
    public void setBadgeValue(int type) {
        if (type == TYPE_NEWS)
            tmpBadgeValue -= 1;
        if (tmpBadgeValue > 0) {
            BadgeDrawable badgeDrawable = ((ActivityMainBinding) binding).bottomNavigationView.getOrCreateBadge(R.id.nav_your_news);
            badgeDrawable.setNumber(tmpBadgeValue);
        } else {
            ((ActivityMainBinding) binding).bottomNavigationView.removeBadge(R.id.nav_your_news);
        }
    }

    private class SingleNewsViewHolder extends RecyclerView.ViewHolder {
        SingleNewsBinding binding;

        SingleNewsViewHolder(@NonNull View itemView, SingleNewsBinding binding) {
            super(itemView);
            this.binding = binding;
        }

        public SingleNewsBinding getBinding() {
            return binding;
        }
    }
}
