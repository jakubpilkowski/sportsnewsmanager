package pl.loando.sportnewsmanager.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.base.BaseActivity;
import pl.loando.sportnewsmanager.databinding.LanguageSpinnerItemBinding;
import pl.loando.sportnewsmanager.models.SpinnerItem;

public class SpinnerItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<SpinnerItem> spinnerItems = new ArrayList<>();
    private ArrayList<SpinnerItemsAdapterViewModel>viewModels = new ArrayList<>();
    private BaseActivity activity;
    private String title;
    public void setSpinnerItems(ArrayList<SpinnerItem> spinnerItems) {
        this.spinnerItems.clear();
        this.spinnerItems.addAll(spinnerItems);
        notifyDataSetChanged();
    }
    public void setActivity(BaseActivity activity){
        this.activity = activity;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.language_spinner_item,parent,false);
        LanguageSpinnerItemBinding binding = LanguageSpinnerItemBinding.bind(view);
        return new ItemHolder(view,binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        SpinnerItemsAdapterViewModel viewModel;
        if(viewModels.size()<=position) {
        viewModel=new SpinnerItemsAdapterViewModel();
            ((ItemHolder) holder).binding.setViewModel(viewModel);
            viewModels.add(viewModel);
        }
        else {
            viewModel = viewModels.get(position);
            ((ItemHolder)holder).binding.setViewModel(viewModel);
        }
        viewModel.init(spinnerItems.get(position),activity,title);
    }

    @Override
    public int getItemCount() {
        return spinnerItems.size();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public class ItemHolder extends RecyclerView.ViewHolder{
        LanguageSpinnerItemBinding binding;
        ItemHolder(@NonNull View itemView, LanguageSpinnerItemBinding binding) {
            super(itemView);
            this.binding=binding;
        }

        public LanguageSpinnerItemBinding getBinding() {
            return binding;
        }
    }
}
