package pl.loando.sportnewsmanager.adapters;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import pl.loando.sportnewsmanager.base.BaseViewModel;
import pl.loando.sportnewsmanager.models.Site;
import pl.loando.sportnewsmanager.navigation.Navigator;

public class PopularSitesListAdapterViewModel extends BaseViewModel {
    static final int VISIBLE = 1;
    static final int GONE = 0;
    private static final int NOTHING = -1;
    public ObservableField<String> url = new ObservableField<>();
    public ObservableField<String> name = new ObservableField<>();
    public ObservableField<String> label = new ObservableField<>();
    public ObservableInt show = new ObservableInt();
    public ObservableInt place = new ObservableInt();
    private Site site;
    private Navigator navigator;

    public void init(Site site, Navigator navigator) {
        this.site = site;
        this.navigator = navigator;
        url.set(site.getUrl());
        name.set(site.getName());
        label.set(site.getLabel());
        show.set(NOTHING);
    }

    void initPlace() {
        place.set(site.getPlace());
    }

    public void onClick() {
        if (show.get() != VISIBLE)
            show.set(VISIBLE);
        else
            show.set(GONE);
    }

    public void onImageClick() {
        navigator.openSite(site.getUri());
    }
}
