package pl.loando.sportnewsmanager.adapters;

import androidx.databinding.ObservableField;

import pl.loando.sportnewsmanager.base.BaseViewModel;
import pl.loando.sportnewsmanager.models.Site;
import pl.loando.sportnewsmanager.models.Team;

public class TeamsManagementAdapterViewModel extends BaseViewModel {
    public ObservableField<String> imageUrl = new ObservableField<>();
    public ObservableField<String> name = new ObservableField<>();
    void initSite(Site site, int position){
         name.set(site.getName());
         imageUrl.set(site.getUrl());
    }
    void initTeam(Team team){
        name.set(team.getName());
        imageUrl.set(team.getUrl());
    }
}
