package pl.loando.sportnewsmanager.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import pl.loando.sportnewsmanager.helpers.JsonTranslator;

public class UserPreferences {

    public static final String DARK_MODE = "DARK_MODE";
    public static final String LANGUAGE = "LANGUAGE";
    public static final String ORDERS = "ORDERS";
    public static final String NOTIFICATIONS = "NOTIFICATIONS";
    public static final String SHOW = "SHOW";
    public static final String PROPOSED_SITES = "PROPOSED_SITES";
    public static final String OVERLAP = "OVERLAP";
    public static final String SETTINGS = "SETTINGS";
    public static final String TEAMS = "TEAMS";
    private static UserPreferences INSTANCE;
    private SharedPreferences sharedPreferences;
    private Gson gson = new Gson();

    public UserPreferences(String file, Context context) {
        sharedPreferences = context.getSharedPreferences(file, Context.MODE_PRIVATE);
    }

    public static UserPreferences get() {
        return INSTANCE;
    }

    public static UserPreferences init(String file, Context context) {
        return INSTANCE = new UserPreferences(file, context);
    }

    public <V> V getValues(String fileName, TypeToken typeToken) {
        String value = sharedPreferences.getString(fileName, "");
        if (value.equals("")) {
            return null;
        }
        return JsonTranslator.getObjectFromJson(value, typeToken);
    }

    public <V> void save(String fileName, V value) {
        String json = JsonTranslator.getJsonFromObject(value);
        sharedPreferences.edit().putString(fileName, json).apply();
    }

    public void addTeam(String fileName, Team value, TypeToken typeToken) {
        String json;
        ArrayList<Team> teams;
        if (!sharedPreferences.getString(fileName, "").isEmpty()) {
            teams = getTeamValues(fileName, typeToken);
            teams.add(value);
        } else {
            teams = new ArrayList<>();
            teams.add(value);
        }
        json = gson.toJson(teams);
        sharedPreferences.edit().putString(fileName, json).apply();
    }

    public void removeTeam(String fileName, int position, TypeToken typeToken) {
        ArrayList<Team> teams = getTeamValues(fileName, typeToken);
        teams.remove(position);
        if (teams.size() != 0) {
            String json = gson.toJson(teams);
            sharedPreferences.edit().putString(fileName, json).apply();
        } else {
            sharedPreferences.edit().putString(fileName, "").apply();
        }
    }

    private <T> T getTeamValues(String fileName, TypeToken typeToken) {
        String storedTeam = sharedPreferences.getString(fileName, "");
        Type type = typeToken.getType();
        return gson.fromJson(storedTeam, type);
    }

}
