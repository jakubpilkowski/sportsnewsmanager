package pl.loando.sportnewsmanager.models;

public class SpinnerItem{
    private int iconRes;
    private String text;

    public SpinnerItem(String text, int iconRes) {
        this.iconRes = iconRes;
        this.text = text;
    }
    public SpinnerItem(String text){
        this.text = text;
        this.iconRes = 0;
    }
    public int getIconRes() {
        return iconRes;
    }

    public String getText() {
        return text;
    }
}
