package pl.loando.sportnewsmanager.models;

public class Team {
    private String name;
    private String url;

    public Team(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }
}
