package pl.loando.sportnewsmanager.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pl.loando.sportnewsmanager.models.ChooserItem;
import pl.loando.sportnewsmanager.models.SpinnerItem;

public class TmpDataBase {

    private static TmpDataBase INSTANCE;

    public static TmpDataBase init(){
        return INSTANCE = new TmpDataBase();
    }
    public static TmpDataBase getINSTANCE() {
        return INSTANCE;
    }
    private  HashMap<Integer,List<ChooserItem>> items = new HashMap<>();
    private int id=1;
    public  void addList(List<ChooserItem>listItem){
        items.put(id,listItem);
        id++;
    }

    public  HashMap<Integer, List<ChooserItem>> getItems() {
        return items;
    }

    public  List<ChooserItem>getValueByKey(int key){
        List<ChooserItem> list = new ArrayList<>();
        list.addAll(items.get(key));
        return list;
    }
}
