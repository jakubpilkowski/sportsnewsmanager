package pl.loando.sportnewsmanager.helpers;

import android.content.Context;

import java.util.ArrayList;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.models.SpinnerItem;
import pl.loando.sportnewsmanager.models.UserPreferences;

public class TmpSettingsDB {
    private ArrayList<SpinnerItem> languages = new ArrayList<>();
    private ArrayList<SpinnerItem> orders = new ArrayList<>();
    private ArrayList<SpinnerItem> overlaps = new ArrayList<>();
    private ArrayList<SpinnerItem> show = new ArrayList<>();


    public TmpSettingsDB(Context context) {
        languages.add(new SpinnerItem(context.getString(R.string.polish), R.drawable.ic_poland));
        languages.add(new SpinnerItem(context.getString(R.string.french), R.drawable.ic_france));
        languages.add(new SpinnerItem(context.getString(R.string.english), R.drawable.ic_united_kingdom));
        languages.add(new SpinnerItem(context.getString(R.string.spanish), R.drawable.ic_spain));
        languages.add(new SpinnerItem(context.getString(R.string.italian), R.drawable.ic_italy));
        languages.add(new SpinnerItem(context.getString(R.string.german), R.drawable.ic_germany));
        orders.add(new SpinnerItem(context.getString(R.string.teams)));
        orders.add(new SpinnerItem(context.getString(R.string.sites)));
        overlaps.add(new SpinnerItem(context.getString(R.string.title_your_news)));
        overlaps.add(new SpinnerItem(context.getString(R.string.title_add_news)));
        overlaps.add(new SpinnerItem(context.getString(R.string.title_popular_sites)));
        show.add(new SpinnerItem("Wszystko"));
        show.add(new SpinnerItem("Wyniki"));
        show.add(new SpinnerItem("Newsy"));
    }

    public ArrayList<SpinnerItem> getLanguages() {
        return languages;
    }

    public ArrayList<SpinnerItem> getOverlaps() {
        return overlaps;
    }

    public ArrayList<SpinnerItem> getOrders() {
        return orders;
    }

    public ArrayList<SpinnerItem> getShow() {
        return show;
    }

    public ArrayList<SpinnerItem> getList(String tag) {
        ArrayList<SpinnerItem> list;
        switch (tag) {
            case UserPreferences.ORDERS:
                list = getOrders();
                break;
            case UserPreferences.LANGUAGE:
                list = getLanguages();
                break;
            case UserPreferences.OVERLAP:
                list = getOverlaps();
                break;
            case UserPreferences.SHOW:
                list = getShow();
                break;
            default:
                list = new ArrayList<>();
        }
        return list;
    }
}
