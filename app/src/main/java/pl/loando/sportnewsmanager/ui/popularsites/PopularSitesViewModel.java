package pl.loando.sportnewsmanager.ui.popularsites;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.adapters.PopularSitesListAdapter;
import pl.loando.sportnewsmanager.base.BaseViewModel;
import pl.loando.sportnewsmanager.models.Site;

public class PopularSitesViewModel extends BaseViewModel {
    public ObservableField<PopularSitesListAdapter> adapter = new ObservableField<>();
    private List<Site> popularSites = new ArrayList<>();
    private PopularSitesListAdapter popularSitesListAdapter = new PopularSitesListAdapter();
    public ObservableInt color = new ObservableInt();

    public void init() {
        color.set(R.color.colorDarkGreen);
        adapter.set(popularSitesListAdapter);
        popularSites.clear();
        popularSites.add(new Site("Football Italia", "Strona poświęcona lidze włoskiej. Wszystkie newsy są po angielsku.", "https://www.football-italia.net", "https://www.football-italia.net/sites/all/themes/italia/logo2.png", 200));
        popularSites.add(new Site("Transfery.info", "Strona poświęcona plotkom transferowym. Strona cechuje się dużą ilością newsów.", "https://transfery.info", "https://transfery.info/img/logo/logo.png", 150));
        popularSites.add(new Site("TransferMarkt", "Strona poświęcona wartościom rynkowym piłkarzy oraz transferom. Strona zawiera dużą bazę statystyk odnośnie transferów.", "https://www.transfermarkt.pl", "https://lh6.ggpht.com/bVb7EM92e__lEy8Al2Pdai9CBMYxT2BjNWJOExpkPKk5zmQmdmKadtU6SpT_XiHvr-0=h300", 30));
        popularSites.add(new Site("Przegląd Sportowy", "Najpopularniejsza strona poświęcona sporcie w Polsce.", "https://www.przegladsportowy.pl", "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Przeglad_Sportowy_logo.svg/1280px-Przeglad_Sportowy_logo.svg.png", 210));
        popularSites.add(new Site("Whoscored.com", "Strona poświęcona statystykom piłkarskim.", "https://www.whoscored.com", "https://d2zywfiolv4f83.cloudfront.net/img/logo.png", 350));
        popularSites.add(new Site("Sport.pl", "Strona poświęcona najnowszym informacjom sportowym", "http://www.sport.pl/sport-hp/0,0.html", "https://bi.im-g.pl/im/0/24265/m24265940.png", 20));
        popularSites.add(new Site("Wirtualna Polska", "Odział WP zajmujący się informacjami sportowymi", "https://sportowefakty.wp.pl", "https://profil.wp.pl/l/images/wp.png", 100));
        Collections.sort(popularSites, Site.SITE_COMPARATOR);
        for (int i = 0; i < popularSites.size(); i++) {
            if (i <= 2) {
                popularSites.get(i).setHotSite(true);
                popularSites.get(i).setPlace(i + 1);
            } else {
                popularSites.get(i).setHotSite(false);
                popularSites.get(i).setPlace(Site.DEFAULT);
            }
        }
        popularSitesListAdapter.setPopularSitesList(popularSites);
        popularSitesListAdapter.setNavigator(getNavigator());
    }

}
