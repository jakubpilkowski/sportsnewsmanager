package pl.loando.sportnewsmanager.ui.settings;

import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

import android.view.View;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.activities.main.MainActivity;
import pl.loando.sportnewsmanager.activities.settings.SettingsActivity;
import pl.loando.sportnewsmanager.base.BaseFragment;
import pl.loando.sportnewsmanager.databinding.ActivityMainBinding;
import pl.loando.sportnewsmanager.databinding.SettingsFragmentBinding;
import pl.loando.sportnewsmanager.navigation.Navigator;

public class SettingsFragment extends BaseFragment<SettingsFragmentBinding, SettingsViewModel> {
    public static final String TAG="SettingsFragment";

    public static SettingsFragment newInstance() {
        Bundle args = new Bundle();
        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }

    @Override
    public void bindData(SettingsFragmentBinding binding) {
        binding.setViewModel(viewModel);
        viewModel.setProviders(this);
        viewModel.init(getContext());
    }

    @Override
    protected Class<SettingsViewModel> getViewModelClass() {
        return SettingsViewModel.class;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.settings_fragment;
    }

    @Override
    public int getHomeTypeButton() {
        return 0;
    }

    @Override
    public int getBackPressType() {
        return 0;
    }

    @Override
    public Navigator getNavigator() {
        return ((SettingsActivity)getActivity()).getNavigator();
    }

    @Override
    public FragmentManager getManager() {
        return getChildFragmentManager();
    }

    @Override
    public ViewDataBinding getBinding() {
        return ((SettingsActivity) getActivity()).getBinding();
    }

    @Override
    public ViewDataBinding getFragmentBinding() {
        return binding;
    }
}
