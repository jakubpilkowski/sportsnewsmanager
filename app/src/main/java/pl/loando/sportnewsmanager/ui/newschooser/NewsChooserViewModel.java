package pl.loando.sportnewsmanager.ui.newschooser;

import androidx.databinding.ObservableField;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import pl.loando.sportnewsmanager.adapters.NewsChooserAdapter;
import pl.loando.sportnewsmanager.base.BaseViewModel;
import pl.loando.sportnewsmanager.helpers.TmpDataBase;
import pl.loando.sportnewsmanager.models.ChooserItem;

public class NewsChooserViewModel extends BaseViewModel {
    // TODO: Implement the ViewModel
    public ObservableField<RecyclerView.Adapter>adapter = new ObservableField<>();
    private NewsChooserAdapter newsChooserAdapter = new NewsChooserAdapter();

    private List<ChooserItem>list = new ArrayList<>();
    void init(int navigateToId){
        adapter.set(newsChooserAdapter);
        list.clear();
        if(navigateToId!=0)
            list.addAll(TmpDataBase.getINSTANCE().getValueByKey(navigateToId));
        newsChooserAdapter.setNavigator(getNavigator());
        newsChooserAdapter.setActivity(getActivity());
        newsChooserAdapter.setItems(list);

    }
}
