package pl.loando.sportnewsmanager.ui.home;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.fragment.app.FragmentPagerAdapter;

import pl.loando.sportnewsmanager.adapters.ViewPagerListAdapter;
import pl.loando.sportnewsmanager.base.BaseViewModel;
import pl.loando.sportnewsmanager.ui.newsmanagement.newsmanagement.NewsManagementFragment;
import pl.loando.sportnewsmanager.ui.popularsites.PopularSitesFragment;
import pl.loando.sportnewsmanager.ui.yournews.YourNewsFragment;

public class HomeViewModel extends BaseViewModel {
    public static final String TAG = "HomeViewModel";
    public ObservableField<ViewPagerListAdapter> pageListAdapter = new ObservableField<>();
    private ObservableInt limit = new ObservableInt();

    public void init() {
        ViewPagerListAdapter pageListAdapter = new ViewPagerListAdapter(getManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        pageListAdapter.addFragment(new YourNewsFragment());
        pageListAdapter.addFragment(new NewsManagementFragment());
        pageListAdapter.addFragment(new PopularSitesFragment());
        this.pageListAdapter.set(pageListAdapter);
        limit.set(3);
    }
}
