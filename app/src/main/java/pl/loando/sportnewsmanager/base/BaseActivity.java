package pl.loando.sportnewsmanager.base;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.navigation.Navigator;
import pl.loando.sportnewsmanager.ui.settings.SettingsFragment;

public abstract class BaseActivity<B extends ViewDataBinding, VM extends BaseViewModel> extends AppCompatActivity {

    public Navigator navigator = new Navigator();
    public VM viewModel;
    public B binding;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        navigator.setActivity(this);
        binding = DataBindingUtil.setContentView(this, getLayoutRes());
        viewModel = ViewModelProviders.of(this).get(getViewModel());
        initActivity(binding);
    }
    public Fragment getCurrentFragment(){
        return getSupportFragmentManager().findFragmentById(getIdFragmentContainer());
    }
    public void refreshHomeButton(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if(getCurrentFragment() instanceof BaseFragment){
            switch (((BaseFragment) getCurrentFragment()).getHomeTypeButton()){
                case 0:
                    getSupportActionBar().setHomeAsUpIndicator(((BaseFragment) getCurrentFragment()).getBackPressIcon());
                    break;
                case 1:
                    getSupportActionBar().setHomeAsUpIndicator(getDrawable(R.drawable.empty_icon));
                    break;
            }
        }
    }
    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1)
            finish();
        else {
            BaseFragment fragment = (BaseFragment) getSupportFragmentManager().findFragmentById(getIdFragmentContainer());
            if (fragment != null)
                switch (fragment.getBackPressType()) {
                    case 0:
                        super.onBackPressed();
                        break;
                    case 1:
//                    navigator.showNewsFragment();
                        break;
                }
        }
    }

    protected abstract void initActivity(B binding);

    public abstract Class<VM> getViewModel();

    public abstract int getIdFragmentContainer();

    @LayoutRes
    public abstract int getLayoutRes();

}
