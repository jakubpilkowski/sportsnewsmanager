package pl.loando.sportnewsmanager.base;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.appbar.AppBarLayout;

import java.lang.reflect.Method;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.activities.main.MainActivity;
import pl.loando.sportnewsmanager.interfaces.Providers;

public abstract class BaseFragment<B extends ViewDataBinding, VM extends BaseViewModel> extends Fragment implements Providers {

    public B binding;
    public VM viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false);
        viewModel = ViewModelProviders.of(this).get(getViewModelClass());
        bindData(binding);
        return binding.getRoot();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getActivity() instanceof MainActivity)
            ((MainActivity) getActivity()).binding.appBar.setExpanded(true, true);
        }
    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        ((BaseActivity) getActivity()).refreshHomeButton();
    }
    public abstract String getFragmentTag();

    public abstract void bindData(B binding);

    protected abstract Class<VM> getViewModelClass();

    @LayoutRes
    public abstract int getLayoutRes();

    public abstract int getHomeTypeButton();
    //0 backPress icon
    //1 nothing

    public int getBackPressIcon() {
        return R.drawable.ic_arrow_back_black_24dp;
    }

    public abstract int getBackPressType();
    //0 backPress
    //1 backToHome


}
